import tkinter as tk
from tkinter import ttk
from bs4 import BeautifulSoup
from urllib import request
import grequests

#comment your code you F***!

class AutocompleteCombobox(tk.ttk.Combobox):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self._hits = []
        self.bind('<KeyRelease>', self.handle_keyrelease)
    
    def autocomplete(self):
 
        self.position = len(self.get())
        # Collect hits
        _hits = []
        for element in self["values"]:
            if element.lower().startswith(self.get().lower()): # Match case insensitively
                _hits.append(element)
        # Update the hit list from previous keystroke
        if _hits != self._hits:
            self._hits=_hits
        # Perform the auto completion
        if self._hits:
            self.delete(0,tk.END)
            self.insert(0,self._hits[0]) #<- with no cycling option the first hit gets displayed
            self.select_range(self.position,tk.END)
                
    def handle_keyrelease(self, event):
        """event handler for the keyrelease event on this widget"""      

        if len(event.keysym) == 1:
            self.autocomplete()


def dict_loop(d, val):

    #Returns the new player's old team name, and ID through lowercase search
    for k, v in d.items():
        for k1, v1 in v.items():
            if k1.lower() == val.lower():
                return (k, v1, k1) #team name, ID, properly capitalized entry
            
    return (None, None, None) #If nothing found return None
    
    
def edit_roster(event=None):

    #Activate roster edit field

    global playerBoxes, playerLabels, textVars, edit, old_players

    edit = True

    for i in range(7):
        playerLabels[i].grid_remove()
        playerBoxes[i].grid()

    old_players = [n.get() for n in textVars]
        
    return
    
def save_roster(event=None):   #tm,n1,n2,n3,n4,n5,n6,n7

    #Writes out any current changes in the roster fields to the playerLinkDict

    global teamName, playerBoxes, playerLabels, textVars, playerLinkDict, old_players, edit
    
    #Prevent errors if edit not pressed before? bad practice tstsst
    if not edit:
        return

    new_players = [n.get() for n in textVars]
    new_dict = {}
    
    for i in range(7):

        prev = old_players[i]
        current = new_players[i]
        
        if prev != current:      #The insert location is not being saved!

            #Move previous into the free agents section
            if prev != "": #If previous was blank, do nothing, else:
                playerLinkDict['.Unassigned'][prev] = playerLinkDict[teamName][prev]

            if current != "": #current as in 'new'; if something is being simpmly removed
                              #there is no need to shuffle in the new entry

                #Find were the new player was previously located
                prev_team, ID, current = dict_loop(playerLinkDict, current)

                if prev_team == None:
                    playerBoxes[i].delete(0, tk.END)
                    playerBoxes[i].insert(0, "Not found!")
                    return
                
                new_dict[current] = ID #Add to the new section !<-This
                del(playerLinkDict[prev_team][current]) #Delete from old section
                                    
                #del(playerLinkDict[teamName][prev]) #Delete replaced from section
                #^We never gonna add 'im in the first place
                
        elif prev != "": #This operation needed to have the newly assigned players be in their respective input pos
            new_dict[prev] = playerLinkDict[teamName][prev]

    #Delete old entry to have a properly ordered new one
    del(playerLinkDict[teamName])
    #Insert new one
    playerLinkDict[teamName] = new_dict

    #Replace search boxes with playerLabels after successful run
    for i in range(7):
        playerBoxes[i].delete(0, tk.END)
        playerBoxes[i].grid_remove()
        playerLabels[i].grid()

    #Enter the roster:
    for i, k in enumerate(new_dict): #new dict {player: ID...}
        playerBoxes[i].insert(0, k)
        
    edit = False
    
    print('Run successful!')
    
    return

def disp_activity(event=None):

    #Returns all the playtime data for a specific global variable team name

    global teamName #<-- fix this global var

    hours = []
    text_str = ''
    prefix = 'https://steamcommunity.com/profiles/'

    rslt_text.set(text_str) #rslt_text by default global?
    
    teamDict = playerLinkDict[teamName]
    for player in teamDict: #Check blank entries
        if teamDict[player] == "None":
            del(teamDict[player])
            
    playerURLs = [teamDict[playerName] for playerName in teamDict]
    playerNames = list(teamDict.keys())
    
    rs = (grequests.get(prefix+url) for url in playerURLs)
    responseList = grequests.map(rs)

    for resp in responseList:
        profileData = resp.content.decode("utf-8", errors="ignore")
        profileHTML = BeautifulSoup(profileData, 'lxml')
        playerHours = profileHTML.find('div',
                class_='recentgame_quicklinks recentgame_recentplaytime')

        if playerHours != None:
            hours.append(str(round(float(playerHours.text.split()[0])))+' h')
        else:
            hours.append('---')
    
    text_str = teamName.title()+'\n.................'
    
    for z in range(len(playerNames)):

        text_str += '\n'+ playerNames[z]+' : '+str(hours[z])
    
        if z == len(playerNames)-1:
            text_str += '\n.................'

    rslt_text.set(text_str)
    
    return

def enter_roster(event=None):

    global teamName, playerBoxes, edit, textVars

    #Check if edit field is on
    if edit:
        
        for i in range(7):
            
            playerBoxes[i].grid_remove()
            playerLabels[i].grid()

    edit = False

    #Retrieve current string found in the combobox (var = tm)
    field = tm.get()

    try:
        #First try using the potentially custom entry of the combobox as a key for the dictionary,
        #If not successful, then go to string variation to check again, if still not found - display that
        
        db_players = list(playerLinkDict[field].keys()) #Keys for the specific team name entry are player names
        
        teamName = field #?
        
    except:
        #Surely there is a more efficient way to find the thing than looping through the whole dictionary?
        found = False
        for key in playerLinkDict:
            if field.lower() == key.lower():
                teamName = key
                found = True
                break

        if found:
            db_players = list(playerLinkDict[teamName].keys())
        
        else:
            cb_team.delete(0, tk.END)
            cb_team.insert(tk.END,"Not found!")
            return

    #Delete the old(current) entries and insert the new:
    #use .delete & .insert to change the textVars, bad method?
        
    for i in range(7):
        playerBoxes[i].delete(0, tk.END)
        try:
            playerBoxes[i].insert(0, db_players[i])
        except:
            continue

def save_exit(filename,data):
    global ctrl_q
    ctrl_q = True
    #data = nested dictionary    
    with open(filename,'w',encoding='utf-8') as fhand:

        for key in sorted(data, key=str.casefold):
            fhand.write("#"+key+";\n")
            for key1, value1 in data[key].items():
                fhand.write(key1+": "+value1+"\n")
            fhand.write("\n")
    try:
        window.destroy()
    except:
        pass
    

#Create window

window = tk.Tk()
window.title("The player database")
window.geometry('640x600')

window.rowconfigure([0, 1], minsize=800, weight=1)
window.columnconfigure([0, 1], minsize=800, weight=1)

#Set up all the frames
search_frame = tk.Frame(window)
team_frame = tk.Frame(search_frame,relief=tk.RIDGE,bd=3)
btm_frame = tk.Frame(window)
edit_frame = tk.Frame(btm_frame,relief=tk.RIDGE,bd=3)
disp_frame = tk.Frame(btm_frame)
results_frame = tk.Frame(disp_frame,height=300,width=200,relief=tk.GROOVE,bd=3)
frm_select = tk.Frame(edit_frame,relief=tk.RAISED,bd=3)
frm_player_btn = tk.Frame(edit_frame)


##stringVarDict = {}
##stringVarDict["tm"] = tk.StringVar()
##for i in range(7):
##    stringVarDict["n"+str(i)] = tk.StringVar()

#Team var + seven possible players

tm = tk.StringVar()
textVars = [tk.StringVar() for i in range(7)]

teamName = None
old_players = None
edit = None
ctrl_q = False
#debug:
new_players = None

rslt_text = tk.StringVar()

#Create selection box for teams
cb_team = AutocompleteCombobox(master=search_frame,width=55,textvariable=tm,
                       font='Helvetica 10 bold')
cb_team.grid(row=0,column=0)
cb_team.insert(0,'Select team...')

#Display the roster in corresponding boxes with enter_roster function using enter box at the end of combobox
btn_enter = tk.Button(search_frame, text='Enter',font='times 15',command=enter_roster)
btn_enter.grid(row=0,column=1,pady=5,padx=5)

#Retrieve all player ID's from the txt file database; the block returns a nested dictionary
#with the structure: {Team name: {Player name: Player ID,...}, ...

with open('Clean_database.txt',encoding='utf-8') as fhand:

    playerList = []
    playerLinkDict = {}
    
    dump = fhand.read()

    teams = list(filter(None, dump.split('#')))
    
    for i in teams:      

        v = list(filter(None, i.split(';')))
        teamName = v[0]

        playerDiv = v[1]

        Players = list(filter(None, playerDiv.split('\n')))# player: link

        playerDict = {}
        for line in Players:

            player = list(filter(None, line.strip().split(': ')))

            playerList.append(player[0])
            
            playerDict[player[0]] = player[1].strip()

        playerLinkDict[teamName] = playerDict       

#Add all of the team names to the previously created combobox
sorted_teams = sorted(playerLinkDict.keys(), key=str.casefold)
cb_team['values'] = sorted_teams
players = playerList #? Clear playerList memory name, redefine to "players".., for what?

#This is just straight up awful, surely there is a better way to intialize the unique combobox objects
lblsize = 'calibri 12'
bullet = "\N{bullet}"
bsize = '20'

#Edit view:
playerBoxes = [AutocompleteCombobox(frm_select, width=20, textvariable=textVars[i]) for i in range(7)]
#Firm playerLabels:
playerLabels = [tk.Label(frm_select, font=lblsize, textvariable=textVars[i]) for i in range(7)]
bullets = [tk.Label(frm_select, text=bullet, font=bsize) for i in range(7)]

for i in range(8):
    if i < 5:
        playerBoxes[i].grid(row=i+1, column=1, pady=10, padx=8)
        playerLabels[i].grid(row=i+1, column=1, pady=10, padx=8, sticky='w')
        bullets[i].grid(row=i+1, column=0, sticky='e')
    #Skip over one
    elif i == 5:      
        divide_lbl = tk.Label(frm_select,text='-'*25)
        divide_lbl.grid(row=i+1,column=1,padx=3,sticky='w')
    else:
        playerBoxes[i-1].grid(row=i+1, column=1, pady=10, padx=8)
        playerLabels[i-1].grid(row=i+1, column=1, pady=10, padx=8, sticky='w')
        bullets[i-1].grid(row=i+1, column=0, sticky='e')
        
        
#Hide the choice boxes until edit is pressed
sorted_abc = sorted(players, key=str.casefold)
sorted_abc.insert(0,"")
for n in playerBoxes:
    n['values'] = sorted_abc
    n.grid_remove()

#Create "Roster:" label, create edit and save buttons to be used later
roster_lbl = tk.Label(edit_frame,text='Roster:',font='Helvetica 12 bold')
btn_save = tk.Button(frm_player_btn, text='Save',font='20',command = save_roster)
btn_edit = tk.Button(frm_player_btn, text='Edit',font='20',command = edit_roster)
btn_save.grid(row=0,column=1,pady=5,padx=5)
btn_edit.grid(row=0,column=0,pady=5,padx=5)

#Attach disp_activity to the "Get activity" button, which will update the rslt_text variable with .set
btn_run = tk.Button(disp_frame, text='Get activity', font='times 20',command = disp_activity)
btn_run.grid(row=0,column=0)

#The results section
results_frame.grid(row=1,column=0,pady=10,padx=10) #TBF
results_lbl = tk.Label(results_frame,font='times 20',textvariable = rslt_text)
results_lbl.grid(row=0, column=0, padx=5, pady=5)

#Pack the frames
roster_lbl.grid(row=0,column=0,sticky='w',pady=5,padx=5)
frm_select.grid(row=1,column=0,sticky='w',padx=10)
frm_player_btn.grid(row=2,column=0,sticky='s',pady=5,padx=5)
edit_frame.grid(row=0,column=0)
disp_frame.grid(row=0,column=1,padx=20)

search_frame.pack(pady=10,padx=10)
btm_frame.pack()

#Event bind shortcuts:
window.bind("<Return>", enter_roster)
window.bind("<Control-Return>", disp_activity)
window.bind("<Control-q>",lambda e: save_exit("Clean_database.txt",playerLinkDict))
cb_team.focus_set()

cb_team.bind("<Control-BackSpace>", lambda e: cb_team.delete(0, tk.END))

for i in range(7):
    playerBoxes[i].bind("<Control-BackSpace>", lambda e, memry = i: playerBoxes[memry].delete(0, tk.END))


window.bind("<Control-s>", save_roster)
window.bind("<Control-e>", edit_roster)

##▒▒▒▒▒▒▒▒▄▄▄▄▄▄▄▄▒▒▒▒▒▒▒▒
##▒▒▒▒▒▄█▀▀░░░░░░▀▀█▄▒▒▒▒▒
##▒▒▒▄█▀▄██▄░░░░░░░░▀█▄▒▒▒
##▒▒█▀░▀░░▄▀░░░░▄▀▀▀▀░▀█▒▒
##▒█▀░░░░███░░░░▄█▄░░░░▀█▒
##▒█░░░░░░▀░░░░░▀█▀░░░░░█▒
##▒█░░░░░░░░░░░░░░░░░░░░█▒
##▒█░░██▄░░▀▀▀▀▄▄░░░░░░░█▒
##▒▀█░█░█░░░▄▄▄▄▄░░░░░░█▀▒
##▒▒▀█▀░▀▀▀▀░▄▄▄▀░░░░▄█▀▒▒
##▒▒▒█░░░░░░▀█░░░░░▄█▀▒▒▒▒
##▒▒▒█▄░░░░░▀█▄▄▄█▀▀▒▒▒▒▒▒
##▒▒▒▒▀▀▀▀▀▀▀▒▒▒▒▒▒▒▒▒▒▒▒▒


window.mainloop()

if not ctrl_q:
    save_exit("Clean_database.txt",playerLinkDict)













